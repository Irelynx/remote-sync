'use strict';
module.exports = {
    dec2hex: function(s) {
        return (s < 15.5 ? '0' : '') + Math.round(s).toString(16);
    },
    hex2dec: function(s) {
        return Number(`0x${s}`);
    },
    hex2str: function(hex) {
        let str = '';
        for (let i = 0; i < hex.length; i += 2) {
            str += String.fromCharCode(this.hex2dec(hex.substr(i, 2)));
        }
        return str;
    },
    leftpad: function(str, len, pad) {
        if (len + 1 >= str.length) {
            str = new Array(len + 1 - str.length).join(pad) + str;
        }
        return str;
    },
    rightpad: function(str, len, pad) {
        if (len + 1 >= str.length) {
            str = str + new Array(len + 1 - str.length).join(pad);
        }
        return str;
    },
    centerpad: function(str, len, pad) {
        if (len + 1 >= str.length) {
            str = new Array(Math.floor((len + 1 - str.length)/2) + 1).join(pad) + str + new Array(Math.floor((len + 1 - str.length)/2 + 1)).join(pad);
        }
        return str;
    },
    restrictLeft: function(str, len=4) {
        if (!str || (str.length <= len)) return str;
        return '..' + str.slice(-(len - 2));
    },
    restrictRight: function(str, len=4) {
        if (!str || (str.length <= len)) return str;
        return str.slice(0, len - 2) + '..';
    },
    restrictCenter: function(str, len=4) {
        if (!str || (str.length <= len)) return str;
        return str.slice(0, len / 2 - 1 + (len % 2) | 0) + '..' + str.slice(-(len / 2 - 1) | 0);
    },
    base26: function(num) {
        const chars = '23456789BCDFGHJKMNPQRTVWXY';
        let output = '';
        const len = 5;
        for (let i = 0; i < len; i++) {
            output += chars[num % chars.length];
            num = Math.floor(num / chars.length);
        }
        if (output.length < len) {
            output = new Array(len - output.length + 1).join(chars[0]) + output;
        }
        return output;
    },
    base32tohex: function(base32) {
        const base32chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
        let bits = '';
        let hex = '';
        let padding = 0;
        for (let i = 0; i < base32.length; i++) {
            if (base32.charAt(i) === '=') {
                bits += '00000';
                padding++;
            }
            else {
                const val = base32chars.indexOf(base32.charAt(i).toUpperCase());
                bits += this.leftpad(val.toString(2), 5, '0');
            }
        }
        for (let i = 0; i + 4 <= bits.length; i += 4) {
            const chunk = bits.substr(i, 4);
            hex = hex + Number(`0b${chunk}`).toString(16);
        }
        // if (hex.length % 2 && hex[hex.length - 1] === '0') {
        //   hex = hex.substr(0, hex.length - 1);
        // }
        switch (padding) {
            case 0:
                break;
            case 6:
                hex = hex.substr(0, hex.length - 8);
                break;
            case 4:
                hex = hex.substr(0, hex.length - 6);
                break;
            case 3:
                hex = hex.substr(0, hex.length - 4);
                break;
            case 1:
                hex = hex.substr(0, hex.length - 2);
                break;
            default:
                throw new Error('Invalid Base32 string');
        }
        return hex;
    },
    secondsToNextUpdateAt: function(datetime, entry) {
        datetime = (datetime || new Date()).getTime() / 1000;
        
        return entry.period - datetime % entry.period;
    },
    tableToConsole: function(table, headers) {
        if ((typeof headers === 'boolean') && headers) {
            headers = table.shift();
        }
        const sizes = (headers || table[0]).map(_=>_.length);
        for (let i=0; i<table.length; i++) {
            for (let j=0; j<table[i].length; j++) {
                if (sizes[j] < table[i][j].length) {
                    sizes[j] = table[i][j].length;
                }
            }
        }
        
        let out = '';
        let rowSplitter = '';
        for (let j=0; j<sizes.length; j++) {
            if (j!==0) {
                if (headers) out += ' | ';
                rowSplitter += '-+-';
            }
            if (headers) out += this.centerpad(headers[j], sizes[j], ' ');
            rowSplitter += this.leftpad('', sizes[j], '-');
        }
        rowSplitter += '\n';
        if (headers) out = rowSplitter + out + '\n' + rowSplitter;
        for (let i=0; i<table.length; i++) {
            for (let j=0; j<sizes.length; j++) {
                if (j!==0) {
                    out += ' | ';
                }
                out += this.rightpad(table[i][j], sizes[j], ' ');
            }
            out += '\n';
        }
        
        console.log(out);
    },
    through2: (function(){
        const Transform = require('stream').Transform;
        const inherits  = require('util').inherits;
        
        function DestroyableTransform(opts) {
            Transform.call(this, opts);
            this._destroyed = false;
        }
        
        inherits(DestroyableTransform, Transform);
        
        DestroyableTransform.prototype.destroy = function(err) {
            if (this._destroyed) return;
            this._destroyed = true;
            
            var self = this;
            process.nextTick(function() {
                if (err) self.emit('error', err);
                self.emit('close');
            });
        }
        
        // a noop _transform function
        function noop (chunk, enc, callback) {
            callback(null, chunk);
        }
        
        
        // create a new export function, used by both the main export and
        // the .ctor export, contains common logic for dealing with arguments
        function through2 (construct) {
            return function (options, transform, flush) {
                if (typeof options == 'function') {
                    flush = transform;
                    transform = options;
                    options = {};
                }
            
                if (typeof transform != 'function')
                    transform = noop;
            
                if (typeof flush != 'function')
                    flush = null;
            
                return construct(options, transform, flush);
            }
        }
        
        
        // main export, just make me a transform stream!
        const _export = through2(function (options, transform, flush) {
            var t2 = new DestroyableTransform(options);
        
            t2._transform = transform;
        
            if (flush)
                t2._flush = flush;
            
            return t2
        });
        
        
        // make me a reusable prototype that I can `new`, or implicitly `new`
        // with a constructor call
        _export.ctor = through2(function (options, transform, flush) {
            function Through2 (override) {
                if (!(this instanceof Through2))
                    return new Through2(override);
            
                this.options = Object.assign({}, options, override);
            
                DestroyableTransform.call(this, this.options);
            }
            
            inherits(Through2, DestroyableTransform);
            
            Through2.prototype._transform = transform;
            
            if (flush)
                Through2.prototype._flush = flush;
            
            return Through2;
        });
        
        
        _export.obj = through2(function (options, transform, flush) {
            var t2 = new DestroyableTransform(Object.assign({ objectMode: true, highWaterMark: 16 }, options));
            
            t2._transform = transform;
            
            if (flush)
                t2._flush = flush;
            
            return t2;
        });
        return _export;
    })()
}