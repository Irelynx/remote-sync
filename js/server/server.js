const http = require('http');
const path = require('path');
const fs = require('fs');
const zlib = require('zlib');
const stream = require('stream');

function isBase64(v, opts) {
    if (v instanceof Boolean || typeof v === 'boolean') {
        return false
    }

    if (!(opts instanceof Object)) {
        opts = {}
    }

    if (opts.allowEmpty === false && v === '') {
        return false
    }

    var regex = '(?:[A-Za-z0-9+\\/]{4})*(?:[A-Za-z0-9+\\/]{2}==|[A-Za-z0-9+\/]{3}=)?'
    var mimeRegex = '(data:\\w+\\/[a-zA-Z\\+\\-\\.]+;base64,)'

    if (opts.mimeRequired === true) {
        regex =  mimeRegex + regex
    } else if (opts.allowMime === true) {
        regex = mimeRegex + '?' + regex
    }

    if (opts.paddingRequired === false) {
        regex = '(?:[A-Za-z0-9+\\/]{4})*(?:[A-Za-z0-9+\\/]{2}(==)?|[A-Za-z0-9+\\/]{3}=?)?'
    }

    return (new RegExp('^' + regex + '$', 'gi')).test(v)
}

const app = {
    XPlanePath: path.resolve('../'),
    compressResponses: true,
    main: async function() {
        console.log('Loading index for X-Plane 11');
        fs.readFile('latest.index', app.readIndex);
        app.server = http.createServer(app.router);
    },
    getMyIP: function(request) {
        // proxy: x-forwarded-for: client, proxy1, proxy2, ...
        return (request.headers['x-forwarded-for'] || '').split(',').pop() || 
            // proxy: X-Real-IP
            request.headers["x-real-ip"] || 
            request.connection.remoteAddress || 
            request.socket.remoteAddress ||
            (request.connection.socket ? request.connection.socket.remoteAddress : null);
    },
    router: async function(request, response) {
        if (!app.indexReady) {
            response.statusCode = 500;
            response.end();
            return;
        }
        if (request.url === '/latest.index') {
            app.sendFile(['',path.normalize(path.join(app.XPlanePath, '@sync/latest.index')), app._indexSize, ''], response, app.compressResponses);
            return;
        }
        // old:
        // if (isBase64(request.url.slice(1), {allowEmpty: false})) {
        //     const data = app.index_findByB64(request.url.slice(1));
        //     if (data) {
        //         app.sendFile(data, response, true);
        //     } else {
        //         // not found in index
        //         response.statusCode = 400;
        //         response.end();
        //     }
        //     return;
        // }
        // new:
        let p = Buffer.from(request.url.slice(1), 'base64').toString();
        if (p.indexOf('../') > -1 || p.indexOf('..\\') > -1) {
            response.statusCode = 400;
            response.end();
            return;
        } else if (app.isValidFilePath(p)) {
            p = path.normalize(path.join(app.XPlanePath, p));
            app.checkFile(p, function(valid){
                if (valid !== undefined && valid !== false) {
                    app.sendFile([request.url.slice(1), p, valid, ''], response, app.compressResponses);
                } else {
                    response.statusCode = 400;
                    response.end();
                }
            });
            return;
        } else {
            console.log('invalid path requested from', app.getMyIP(request));
            request.socket.destroy();
        }
        
        response.statusCode = 400;
        response.end();
    },
    checkFile: function(filePath, cb) {
        fs.stat(filePath, function(error, stat) {
            if (error) {
                cb(false);
            } else if (stat.isFile()) {
                cb(stat.size);
            } else {
                cb(false);
            }
        });
    },
    isValidFilePath: function(filePath) {
        try {
            filePath = decodeURIComponent(filePath);
        } catch(e) {
            return false;
        }
        if (filePath.indexOf('\0') > -1) {
            return false;
        }
        filePath = path.normalize(path.join(app.XPlanePath, filePath));
        if (filePath.indexOf(app.XPlanePath) !== 0) {
            return false;
        }
        return true;
    },
    sendFile: function(data, response, compress) {
        const filePath = data[1];
        const size = data[2];
        const rstream = fs.createReadStream(filePath);
        function onError(error) {
            if (error) {
                console.error(error);
                response.end();
            }
        }
        if (compress) {
            if (size > -1) {
                response.writeHead(200, {
                    'Content-Type': 'application/octet-stream',
                    // Content-Length emits premature-close if closed earlier
                    'X-Content-Length': size,
                    'Content-Encoding': 'gzip'
                });
            } else {
                response.writeHead(200, {
                    'Content-Type': 'application/octet-stream',
                    'Content-Encoding': 'gzip'
                });
            }
            stream.pipeline(rstream, zlib.createGzip(), response, onError)
        } else {
            if (size > -1) {
                response.writeHead(200, {
                    'Content-Type': 'application/octet-stream',
                    'Content-Length': size
                });
            } else {
                response.writeHead(200, {
                    'Content-Type': 'application/octet-stream'
                });
            }
            stream.pipeline(rstream, response, onError);
        }
        
    },
    readIndex: function(error, data) {
        if (error) {
            console.error('X-Plane 11 index can\'t be loaded.', error);
            return;
        }
        app._indexSize = data.length;
        const idx = data.toString().split('\n').map(_=>_.split(' '));
        for (let i=0; i<idx.length; i++) {
            idx[i][1] !== undefined && app.index.push([
                idx[i][0],
                Buffer.from(idx[i][0], 'base64').toString(),
                parseInt(idx[i][1], 36),
                idx[i][2]
            ]);
        }
        app.indexReady = true;
        console.log('X-Plane 11 index loaded');
        app.server.listen(8011, function() {
            console.log('Server listening on localhost:8011');
        });
        
        delete idx;
        delete data;
    },
    indexReady: false,
    _indexSize: 0,
    index: [],
    index_findByB64: function(b64){
        const idx = app.index;
        for (let i=0; i<idx.length; i++) {
            if (idx[i][0] === b64) {
                return idx[i];
            }
        }
    },
    index_findByB64AndHash: function(b64, hash) {
        const idx = app.index;
        for (let i=0; i<idx.length; i++) {
            if (idx[i][0] === b64 && idx[i][3] === hash) {
                return idx[i];
            }
        }
    }
}

app.main(process.argv.slice(2));