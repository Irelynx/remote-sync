# Remote-sync server

Yet-another synchronizer for your files with remote side

Originally written by [Irelynx](https://github.com/Irelynx) in November 2018 for synchronization of XPlane flight simulator mods folders

## Table of contents <a name="toc"></a>

0. [Table of contents](#toc)
1. [Introduction](#introduction)
1. [Indexing](#indexing)
1. [Serving](#serving)
1. [`./latest.index` example](#latest_index_example)
1. [Indexing, Serving and Transmission diagram](#diagram_view)
    
## Introduction <a name="introduction"></a>

Server side of synchronizer has `index.js` and `server.js` files (except `utils.js`)

Expected, that user starts `indexing` or `serving` process from directory INSIDE the target directory.

Example of files tree with `index.js` and `server.js` files:
```
TargetFolder/               <<<--- target folder to sync
             someFile
             @sync/         <<<--- folder with server files (also must be in server exclusion list)
                   index.js
                   server.js
                   utils.js
                   index.bat
                   start server.bat
             someDirectory/
                           ...
```

## Indexing <a name="indexing"></a>

Using `index.js`, user can start `indexing` process.

In function `listFilter` inside `index.js` user can find-out exclusion list (you can write complex rules exclude some files). This function receives relative `filePath` from target folder to currently processing file

Starting `indexing` is simple:
```batch
node ./index.js
```

It's strongly recommended to SHUTDOWN and DO NOT START the `server` until the `indexing` process is not finished, because `server` uses `./latest.index`, generated by `index.js`, and load it once - on startup. So, after each indexing, you MUST restart the server to ensure that `./latest.index` on `server` in actual state

`Indexing` always use `fast` checking (based on only size, to boost and speed-up `indexing` process) except following cases:
* User calls indexer with argument `full`: 
    ```
    node ./index.js full
    ```
* `./latest.index` is not found in server folder

`Full` checking will calculate hash-sum of each file, which can took some time..

In `XPlaneDir` you can set target folder for indexer (by default it is `../`), but it's not recommended.

**NOTE**: If you want to change target path (`XPlaneDir`), do not forget to change same option in `server.js` called `app.XPlanePath` to same value as in `XPlaneDir`

## Serving <a name="serving"></a>

Using `server.js`, user can start `serving` process.

Starting `serving` is simple:
```batch
node ./server.js
```

In `app.compressResponses` inside `server.js` user can set usage of compression mode (true - compression will be used during transmission, false - raw bytes will be sent)

In `app.XPlanePath` you can set target serving directory (by default it is `../`), but it's not recommended.

**NOTE**: If you want to change target path (`app.XPlanePath`), do not forget to change same option in `index.js` before (option called `XPlaneDir` to same value as in `app.XPlanePath`)

## Example of `latest.index` file: <a name="latest_index_example"></a>
    ```
    QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBLmFjZg== 1jcfb 0a15df640e2553255f0ebaaacc8bbe4572f66612
    QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBX2ljb24ucG5n 1m27 e9259948055c96580acc58daf238520c7c0434a9
    QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBX2ljb24xMS5wbmc= 3j3p 6c7cb625bd1523aa3630eb8455eb100fc100064f
    QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBX2ljb24xMV90aHVtYi5wbmc= 8tj 301b01ee07f9a9b4eadcf54445f033f3e72ef250
    ```

## Parsing of `./latest.index` <a name="parsing_latest_index"></a>
* Each row (delimiter - `\n` (line feed)) contains 3 fields: `filePath`, `size` and `hash`, delimited by one space (` `)
* `filePath` appears first, and it is Base64 encoded. 
    First row from example can be decoded like this: 
    ```javascript
    Buffer.from('QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBLmFjZg==', 'base64').toString()
    // -> 'Aircraft/Extra Aircraft/B-52G NASA/B-52G NASA.acf'
    Buffer.from('Aircraft/Extra Aircraft/B-52G NASA/B-52G NASA.acf').toString('base64')
    // -> 'QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBLmFjZg=='
    ```
    **NOTE**: `filePath` is always use UNIX-style directory delimiters (forward slash - `/`)

* `size` in `latest.index` is an Base36 encoded number of Bytes (file size).
    First row from example can be decoded like this:
    ```javascript
    parseInt('1jcfb', 36);
    // -> 2582183 (so, file size is approximately 2.46 MB)
    2582183.toString(36);
    // -> '1jcfb'
    ```
        
    **NOTE**: If `size` is empty, then it must be interpreted as `0` or `Infinity` (`size` can't be determined, or client must re-download this file each time)

* `hash` is just hash in hexadecimal format. In example (`0a15df640e2553255f0ebaaacc8bbe4572f66612`), **sha1** is used (algorithm can be changed in source code).
        
    **NOTE**: If `hash` is empty, then it must be interpreted as empty string (`hash` can't be determined, or client must re-download this file each time)

## Indexing and Serving diagram (with Data transmission diagram) <a name="diagram_view"></a>

```mermaid

sequenceDiagram
    participant FS as File System
    participant S as Server
    participant C as Client
    Note over FS,S: File system interaction
    Note over C,S: Network interaction (via HTTP)
    rect rgba(0, 0, 255, 0.1)
        Note over FS,S: Indexing
        loop Recursively get file paths and size
            S->>+FS: Get directory entries
            FS->>-S: Directory entries
            loop Get files size
                S->>+FS: 
                FS->>-S: File size
                alt if Fast mode
                    S-->>S: Compare size
                    alt if sizes Not equal
                        S-->>FS: Compute hash
                        FS-->>S: 
                        S-->>S: Add new entry
                    end
                else if Full mode
                    S-->>FS: Compute hash
                    FS-->>S: 
                    S-->>S: Compare hash or add new entry
                end
            end
            Note left of S: Repeat for each directory entry recursively
        end
    end
    rect rgba(0, 255, 0, 0.1)
        Note over FS,C: Serving
        Note left of S: Loading and parsing ./latest.index
        S->>+FS: Read /latest.index
        FS->>-S: /latest.index contents
        S-->>S: latest.index parsing
        Note over S: Server Ready
        C->>+S: HTTP GET /QWlyY3JhZnQvR...
        S-->>FS: Create read stream (open for read)
        FS-->>S: 
        S-->>C: HTTP headers
        loop while not EOF
            FS->>S: Chunk
            alt compression allowed
                S-->>S: Stream compression
                S-->>C: Chunk (can be delayed by   compressor)
            else else
                S->>C: Chunk
            end
        end
        S-->>FS: Close read stream (close file descriptor)
        FS-->>S: 
        S->>-C: Connection close
    end
```