'use strict';

const XPlaneDir = '..';

const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const utils = require('./utils.js');

function promiseState(promise, callback) {
  // Symbols and RegExps are never content-equal
  var uniqueValue = globalThis['Symbol'] ? Symbol('unique') : /unique/

  function notifyPendingOrResolved(value) {
    if (value === uniqueValue) {
      return callback('pending')
    } else {
      return callback('fulfilled')
    }
  }

  function notifyRejected(reason) {
    return callback('rejected')
  }
  
  var race = [promise, Promise.resolve(uniqueValue)]
  Promise.race(race).then(notifyPendingOrResolved, notifyRejected)
}

async function getFiles(dir) {
  const dirents = await fs.promises.readdir(dir, { withFileTypes: true });
  const files = await Promise.all(dirents.map((dirent) => {
    const res = path.resolve(dir, dirent.name);
    return dirent.isDirectory() ? getFiles(res) : res;
  }));
  return Array.prototype.concat(...files);
}

function toSeconds(hrtime, precision=3) {
    return (hrtime[0] + hrtime[1] / 1e9).toFixed(precision) + 's';
}
function toTime(hrtime) {
    if (typeof hrtime === 'number') {
        hrtime = [hrtime / 1e3 | 0, hrtime % 1e3];
    } else if (hrtime instanceof Date) {
        hrtime = [hrtime.getTime() / 1e3 | 0, hrtime.getTime() % 1e3];
    }
    return (hrtime[0] > 86400 ? (hrtime[0] / 86400 % 7) + 'w ' : '') +
    (hrtime[0] > 3600 ? (hrtime[0] / 3600 % 24 | 0) + 'h ' : '') +
    (hrtime[0] > 60 ? (hrtime[0] / 60 % 60 | 0) + 'm ' : '') +
    (hrtime[0] % 60 | 0) + 's ' + 
    (hrtime[1] / 1e6 | 0) + 'ms ' + 
    (hrtime[1] / 1e3 % 1e3 | 0) + 'μs ' + 
    (hrtime % 1e3 | 0) + 'ns';
}

async function getSimpleHash(filePath) {
    const stat = await fs.promises.stat(filePath);
    // stat.size
    return stat.size.toString(36); // base-36 size
}
async function getSize(filePath) {
    const stat = await fs.promises.stat(filePath);
    // stat.size
    return stat.size; // base-36 size
}

function getRealHash(filePath, algorithm='sha1') {
    return new Promise((resolve, reject)=>{
        const hash = crypto.createHash(algorithm);
        const stream = fs.ReadStream(filePath);
        stream.on('data', (data)=>{
            hash.update(data);
        });
        stream.on('end', ()=>{
            resolve(hash.digest('hex'));
        });
    });
}

function flushRow() {
    process.stdout.write('\r' + ' '.repeat(process.stdout.columns));
}

function Log() {
    if (!this || !(this instanceof Log)) {
        return new Log();
    }
    this.type = '';
    this.data = [];
    this.interval = 0;
    const that = this;
    this.templatesL = {};
    this.templatesR = {};
    this.primaries = {};
    
    function replaceT(template, data) {
        let out = '' + template;
        for (let i=0; i<data.length; i++) {
            out = out.split('$'+i).join(data[i]);
        }
        return out;
    }
    
    this.registerTemplate = function(type, template, position='Left', primary=true) {
        if (position === 'Left') {
            this.templatesL[type] = template;
        } else if (position === 'Right') {
            this.templatesR[type] = template;
        } else {
            return;
        }
        
        if (primary) {
            this.primaries[type] = position;
        }
        if (!this.primaries[type]) {
            this.primaries[type] = 'Right';
        }
    }
    
    this.update = function(type, ...data) {
        this.type = type;
        this.data = data;
    };
    this.intFunction = function() {
        let left = '';
        let right = '';
        const size = process.stdout.columns;
        if (that.templatesL[that.type]) {
            left = replaceT(that.templatesL[that.type], that.data);
        }
        if (that.templatesR[that.type]) {
            right = replaceT(that.templatesR[that.type], that.data);
        }
        
        if (left.length + right.length > size) {
            if (that.primaries[that.type] === 'Left') {
                let len = size - left.length - 1;
                right = utils.restrictCenter(right, len);
            } else { // Right
                let len = size - right.length - 1;
                left = utils.restrictCenter(left, len);
            }
        }
        
        if (that.primaries[that.type] === 'Left') {
            process.stdout.write('\r' + (left + ' ' + utils.rightpad(right, size - left.length - 1, ' ')).slice(0, size));
        } else {
            process.stdout.write('\r' + (utils.rightpad(left, size - right.length - 1, ' ') + ' ' + right).slice(0, size));
        }
    }
    this.start = function() {
        this.interval = setInterval(this.intFunction.bind(this), 100);
    }
    this.stop = function() {
        clearInterval(this.interval);
        this.intFunction.bind(this)();
    }
}

async function main(args) {
    const opts = {fast: true, full: false};
    if (args.indexOf('full') > -1) {
        opts.full = true;
        opts.fast = false;
    }
    if (args.indexOf('fast') > -1) {
        opts.full = false;
        opts.fast = true;
    }
    
    let idx = [];
    if (opts.fast) {
        console.log('Loading latest index...');
        try {
            idx = (await fs.promises.readFile('latest.index')).toString().split('\n').map(_=>{_ = _.split(' '); return [_[0], parseInt(_[1] || 0, 36), _[2]]});
        } catch(e) {
            if (e.code === 'ENOENT') {
                console.info('Latest index not found, start full scanning');
                opts.full = true;
                opts.fast = false;
            }
        };
    }
    
    const log = Log();
    log.registerTemplate('hashing', '$2', 'Left', false);
    log.registerTemplate('hashing', '$3 $0/$1', 'Right', true);
    
    console.log('Reading XPlane directories ('+ path.resolve(XPlaneDir) +')...');
    let timer_start = process.hrtime();
    const list = await getFiles(XPlaneDir);
    let timer_end = process.hrtime(timer_start);
    console.log(list.length + ' files founded in ' + toSeconds(timer_end));
    
    const hashList = [];
    let skipped = 0;
    let fast_updates = 0;
    let full_updates = 0;
    
    if (opts.fast) {
        console.log('fast sync with index (no hashing. use "full" argument to force hashing)');
    }
    log.start();
    for (let i = 0; i < list.length; i++) {
        const posixPath = path.relative(XPlaneDir, list[i]).replace(/\\/g, '/');
        if (!listFilter(posixPath)) {
            skipped++;
            continue;
        }
        const b64Path = Buffer.from(posixPath).toString('base64');
        const size = await getSize(list[i]);
        log.update('hashing', i, list.length, posixPath, size);
        if (opts.fast) {
            let flag = false;
            for (let i=0; i<idx.length; i++) {
                if (idx[i][0] === posixPath && idx[i][1] === size) {
                    hashList.push([
                        idx[i][0],
                        idx[i][1].toString(36),
                        idx[i][2]
                    ]);
                    flag = true;
                    break;
                }
            }
            if (flag) {
                fast_updates++;
                continue;
            }
        }
        // new file or update
        hashList.push([
            b64Path,
            size.toString(36),
            await getRealHash(list[i], 'sha1')
        ]);
        full_updates++;
    }
    log.stop();
    
    console.log('complete. skipped: ' + skipped + ' fast: '+ fast_updates + ' full: ' + full_updates);
    
    let out = '';
    
    for (let i = 0; i < hashList.length; i++) {
        out += hashList[i].join(' ') + '\n';
    }
    
    await fs.promises.rename('latest.index', 'index.old');
    await fs.promises.writeFile('latest.index', out);
}

function listFilter(element) {
    if (
        element.startsWith('Output') ||
        element.startsWith('@') ||
        element === 'WorldEditor.exe' ||
        element === 'tileservers.txt' || 
        element.startsWith('Log')
    ) {
        return false;
    }
    return true;
}

main(process.argv.slice(2));