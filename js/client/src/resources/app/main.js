'use strict';
// deskgap builded without inspector..
//const inspector = require('inspector');
//inspector.open();
//inspector.waitForDebugger();

let deskgap;
try {
    deskgap = require('deskgap');
} catch(e) {
    console.log('Console mode');
}
const fs = require('fs');
const path = require('path');
const util = require('util');
const crypto = require('crypto');
const zlib = require('zlib');
const stream = require('stream');
const http = require('http');
const utils = require('./utils.js');

function readJSONSafe(data) {
    var out = {};
    if (data instanceof Buffer) {
        data = data.toString();
    }
    if (typeof data === 'string' && data.indexOf('"') > -1) {
        try {
            out = JSON.parse(data);
        } catch(e) {
            console.log(e);
            out = undefined;
        }
    } else {
        out = data;
    }
    return out;
}

const mockFunction = function() {};

function sleep(time){
    return new Promise((R)=>{setTimeout(R, time);});
}
async function randomSleep(time, probability) {
    if (Math.random() < probability) {
        await sleep(time);
    }
}

const app = {
    server: 'localhost:8011',
    //server: '192.168.40.2:8011',
    status: {
        action: '',
        speed: 0,
        ETA: '',
        progress: {
            current: {
                value: -1,
                info: '...'
            },
            all: {
                value: -1,
                info: '...'
            }
        }
    },
    _status: {},
    _updateStatus: mockFunction,
    logInterval: 100 /* ms */,
    logIntervalFunction: function() {
        app._updateStatus(app.status);
        app._logTarget.send('NodeMessage', {type: 'status', status: app.status});
    },
    _logTarget: {},
    _logInt: 0,
    IPCResolver: function(event, message) {
        event.sender.send('NodeMessage', message);
    },
    _checkType: 'fast',
    check: async function(event, message) {try{
        const msg = readJSONSafe(message);
        if (!msg || !msg.path) {
            event.sender.send('NodeMessage', {type: 'error', error: 'XPlane path is not set.' + JSON.stringify(msg)});
            return;
        }
        if (!msg || !msg.checkType || ['fast', 'full'].indexOf(msg.checkType) === -1) {
            event.sender.send('NodeMessage', {type: 'error', error: 'Check type is not set or is not expected.' + JSON.stringify(msg)});
            return;
        }
        
        if (app._logInt) {
            clearInterval(app._logInt);
            app._logInt = 0;
        }
        app._logTarget = event.sender;
        app._logInt = setInterval(app.logIntervalFunction, app.logInterval);
        
        // step 0 - Prepare

        app.status.action = 'Prepare';
        app.status.progress.all.value = 0;
        app.status.progress.all.info = (msg.checkType === 'full'?'Full':'Fast')+' Check & Update';
        app._checkType = msg.checkType;
        app.status.progress.current.value = -1;
        app.status.progress.current.info = 'Reading X-Plane 11 folder';
        
        const list_absolute = await app.getAllFilesList(msg.path);
        const list = [];
        const sizesList = [];
        
        app.status.progress.all.value = 1;
        
        // step 1 - Reading all files and it's sizes
        
        app.status.action = 'Reading files';
        app.status.speedUnit = 'kEl/s';
        app._updateStatus = app.check_updateStatus_step1;
        //const time1 = Date.now();
        const len = list_absolute.length;
        app._status = {t: Date.now(), i: 0, len: len};
        for (let i=0; i<len; i++) {
            const relPath = path.relative(msg.path, list_absolute[i]).replace(/\\/g, '/');
            list.push(relPath);
            sizesList.push(await app.getSize(list_absolute[i]));
            
            app._status.i = i;
        }
        
        // step 2 - loading index from server
        app.status.action = 'Fetching index';
        app.status.speedUnit = 'MB/s';
        app._updateStatus = app.check_updateStatus_step2;
        app._status = {len: -1, loaded: -1, t: Date.now(), t_ping: 0, t_pong: 0};
        app.status.progress.current.value = -1;
        app.status.progress.current.info = 'Loading 0/1 0.00 MB';
        app.status.progress.all.value = 5;
        app.status.ETA = '~';
        app.status.speed = 0;
        
        app.index = await app.loadIndexFromServer();
        
        if (app._status.len === -1) {
            app.status.progress.all.value = 6;
        }
        
        // step 3 - comparing list, sizesList with app.index;
        // if type == full, and size == idx.size -> hash file
        
        app.status.action = 'Comparing';
        app._updateStatus = app.check_updateStatus_step3;
        app.status.progress.current.value = -1;
        app.status.progress.current.info = 'Comparing with index';
        app.status.ETA = '~';
        app.status.speed = 0;
        let t = 0;
        for (let i=0; i<sizesList.length; i++) {
            t += sizesList[i];
        }
        app._status = {t: Date.now(), i: 0, len: list.length, readed: -1, size: -1, totalSize: t, totalComplete: 0};
        
        const idx = app.index;
        for (let i=0; i<list.length; i++) {
            app._status.i = i;
            let flag = false;
            for (let j=0; j<idx.length; j++) {
                if (
                    idx[j] === undefined || idx[j][0] === undefined || // empty
                    (
                        idx[j][1] === list[i] && // same name
                        ( // same size
                            idx[j][2] === sizesList[i] ||
                            // full
                            msg.checkType === 'full' &&
                            idx[j][2] !== sizesList[i] &&
                            // for _status
                            (flag = true) && false
                        ) &&
                        ( // same hash
                            // fast
                            msg.checkType === 'fast' ||
                            // full
                            msg.checkType === 'full' &&
                            (app._status.size = sizesList[i]) > 0 &&
                            idx[j][3] === await app.getRealHash(path.normalize(path.join(msg.path, list[i])), 'sha1')
                        )
                    )
                ) {
                    //c = idx[j];
                    idx.splice(j, 1);
                    break;
                }
            }
            if (flag) {
                app._status.totalComplete += sizesList[i];
            }
            await randomSleep(0, 0.001);
        }
        
        
        // step 4 - downloading data
        app.status.action = 'Updating data';
        app._updateStatus = app.check_updateStatus_step4;
        app.status.progress.current.value = -1;
        app.status.progress.all.value = 31;
        app.status.progress.current.info = 'Downloading files';
        app.status.ETA = '~';
        app.status.speed = 0;
        
        let totalSize = 0;
        for (let i=0; i<idx.length; i++) {
            if (idx[i] === undefined) {
                idx.splice(i, 1);
                continue;
            }
            totalSize += idx[i][2];
        }
        app._status = {
            files: idx.length, // total files
            current: -1, // current file
            loaded: -1, // loaded size (current)
            loadedReal: -1, // loaded size (current, compressed)
            len: -1, // total size (current)
            loadedRealTotal: 0, // total loaded compressed size
            loadedTotal: 0, // total loaded decompressed size
            totalSize: totalSize, // total decompressed target size (overall)
            t: Date.now(), t_ping: Date.now(), t_pong: Date.now(),
            currentPath: {}
        };
        
        for (let i=0; i<idx.length; i++) {
            app._status.current = i;
            const filePath = path.normalize(path.join(msg.path, idx[i][1]));
            app._status.currentPath = path.parse(filePath);
            await app.streamWriteFromNet('http://' + app.server + '/' + idx[i][0], filePath);
        }
        
        // completed 
        
        app.status.action = 'Complete';
        app.status.progress.all.value = 100;
        app.status.progress.all.info = 'Up-to-date';
        app.status.progress.current.value = 100;
        app.status.progress.current.info = ' ';
        app.status.ETA = '-';
        app.status.speed = 0;
        app.status.speedUnit = ' ';
        
        await sleep(0);
        
        app._updateStatus = mockFunction;
        app.logIntervalFunction();
        if (app._logInt) {
            clearInterval(app._logInt);
            app._logInt = 0;
        }
        
        await sleep(app.logInterval);
        // end
        event.sender.send('NodeMessage', {type: 'info', info: 'Your data is up-to-date'});
        if (msg.checkType === 'fast') {
            event.sender.send('FastCheckEnd', true);
        } else if (msg.checkType === 'full') {
            event.sender.send('FullCheckEnd', true);
        }
    } catch(e){
        // ToDo: improve handling errors
        app.status.action = 'ERROR';
        app.status.ETA = '~';
        app._updateStatus = mockFunction;
        event.sender.send('NodeMessage', {type: 'error', error: e.message + '<br><code>' + e.stack.split('\n').join('<br>') + '</code>'
    });}},
    check_updateStatus_step1: function(stat) {
        const now = Date.now();
        const t = app._status.t;
        const i = app._status.i;
        const len = app._status.len;
        stat.progress.current.value = (i+1) / len * 100;
        stat.progress.current.info = utils.restrictCenter('Reading ' + (i+1) + '/' + len, 34);
        stat.progress.all.value = 1 + stat.progress.current.value / 100 * 4;
        stat.speed = i / (now - t);
        stat.ETA = app.toTime(now - (t + len / stat.speed), 4);
    },
    check_updateStatus_step2: function(stat) {
        const len = app._status.len;
        const loaded = app._status.loaded;
        const t = app._status.t;
        const now = Date.now();
        // ping: app._status.t_pong - app._status.t_ping
        
        if (loaded > -1) {
            stat.speed = loaded / (now - t) / 1024; // MB/s
            stat.progress.current.info = 'Loading 1/1 ' + (loaded / 1024 / 1024).toFixed(2) + (len > -1 ? '/' + (len / 1024 / 1024).toFixed(2) : '') + ' MB';
        }
        if (len > -1) {
            stat.progress.current.value = loaded / len * 100;
            stat.progress.all.value = 5 + stat.progress.current.value / 100 * 1;
            if (loaded > -1) {
                stat.ETA = app.toTime(now - (t + len / (stat.speed << 10)), 4);
            }
        }
    },
    check_updateStatus_step3: function(stat) {
        const now = Date.now();
        const t = app._status.t;
        if (app._status.size > -1) {
            stat.progress.current.value = app._status.readed / app._status.size * 100;
        } else {
            stat.progress.current.value = (app._status.i + 1) / app._status.len * 100;
        }
        stat.progress.current.info = 'Comparing ' + (app._status.i+1) + '/' + app._status.len;
        stat.progress.all.value = 6 + ((app._status.i + 1) / app._status.len) * 19;
        if (app._status.len > -1) {
            stat.speed = app._status.totalComplete / (now - t) / 1024;
            stat.ETA = app.toTime(t + (app._status.totalSize / 1024 / 1024) / stat.speed * 1000 - now, 4);
        }
    },
    check_updateStatus_step4: function(stat) {
        const now = Date.now();
        const t = app._status.t;
        stat.speed = app._status.loadedTotal / (now - t) / 1024;
        // hack
        stat.speedUnit = '('+ (app._status.loadedRealTotal / (now - t) / 1024).toFixed(2) + ') MB/s';
        //stat.ETA = new Date(t + app._status.totalSize / (stat.speed * 1024 * 1024) | 0);
        stat.ETA = app.toTime(t + (app._status.totalSize / 1024 / 1024) / stat.speed * 1000 - now, 4);
        if (app._status.len === -1) {
            stat.progress.current.value = -1;
        } else {
            stat.progress.current.value = app._status.loaded / app._status.len * 100;
        }
        //stat.progress.current.info = 'Updating ' + (app._status.loaded > -1 ? (app._status.loaded / 1024 / 1024).toFixed(2) : 0) + ' (' + (app._status.loadedReal > -1? (app._status.loadedReal / 1024 / 1024).toFixed(2) : 0) + ') / ' + (app._status.len > -1 ? (app._status.len / 1024 / 1024).toFixed(2) : '?') + ' MB';
        stat.progress.current.info = ' ' + (app._status.loaded > -1 ? (app._status.loaded / 1024 / 1024).toFixed(2) : 0) + ' of ' + (app._status.len > -1 ? (app._status.len / 1024 / 1024).toFixed(2) : '?') + ' MB';
        stat.progress.current.info = app._status.currentPath.base.slice(0, Math.max(0, 60 - stat.progress.current.info.length)) + stat.progress.current.info;
        //stat.progress.all.value = 31 + ((app._status.loadedTotal || 1) / (app._status.totalSize || 1)) * 69;
        stat.progress.all.value = ((app._status.loadedTotal || 1) / (app._status.totalSize || 1)) * 100;
        if (app._status.totalSize > (1 << 30)) {
            stat.progress.all.info = 'Total: ' + ((app._status.current+1) + ' of ' + app._status.files) + ' - ' + (app._status.loadedTotal / 1024 / 1024 / 1024 || 1).toFixed(2) + ' of ' + (app._status.totalSize / 1024 / 1024 / 1024 || 1).toFixed(2) + ' GB';
        } else {
            stat.progress.all.info = (app._checkType === 'full'?'Full':'Fast')+' Check & Update - ' + (app._status.loadedTotal / 1024 / 1024 || 1).toFixed(2) + ' of ' + (app._status.totalSize / 1024 / 1024 || 1).toFixed(2) + ' MB';
        }
    },
    streamWriteFromNet: function(from, to) {
        return new Promise((resolve, reject)=>{
            fs.promises.mkdir(path.parse(to).dir, {recursive: true})
            .then(()=>app.getStreamFromNet(from))
            .then((gunzippedStream)=>{
                app._status.loaded = 0;
                app._status.loadedReal = 0;
                // app._status.len // already setted by getStreamFromNet
                function callback(error) {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                }
                const wstream = fs.createWriteStream(to);
                // stream pipeline
                stream.pipeline(
                    gunzippedStream,
                    utils.through2(function(chunk, enc, cb) {
                        app._status.loadedTotal += chunk.length; // decompressed chunk size
                        app._status.loadedRealTotal += gunzippedStream.bytesWritten - app._status.loadedReal;
                        app._status.loadedReal = gunzippedStream.bytesWritten;
                        app._status.loaded += chunk.length;
                        // forward chunk to pipeline query
                        this.push(chunk);
                        cb();
                    }), 
                    wstream, 
                    callback
                );
                // gunzippedStream
                // .pipe(utils.through2(function(chunk, enc, callback) {
                //     app._status.loadedTotal += chunk.length; // decompressed chunk size
                //     app._status.loadedRealTotal += gunzippedStream.bytesWritten - app._status.loadedReal;
                //     app._status.loadedReal = gunzippedStream.bytesWritten;
                //     app._status.loaded += chunk.length;
                //     // forward chunk to pipeline query
                //     this.push(chunk);
                //     callback();
                // }))
                // .pipe(fs.createWriteStream(to))
                // .on('finish', ()=>{
                //     resolve();
                // })
                // .on('error', (e)=>{
                //     reject(e);
                // });
            })
            .catch(reject);
        });
    },
    getFromNet: function(url) {
        return new Promise((resolve, reject)=>{
            app._status.t_ping = Date.now();
            http.get(url, function(response) {
                app._status.t_pong = Date.now();
                if (response.statusCode === 200) {
                    // do not use Buffer.alloc(0) and then Buffer.concat([data, chunk])
                    // use faster alternative - push buffers in array, on end use Buffer.concat(list, BuffersLength);
                    const data = [];
                    let size = 0;
                    if (response.headers['content-encoding'] === 'gzip') {
                        const gzip = zlib.createGunzip();
                        stream.pipeline(response, gzip, (e)=>{e && reject(e)});
                        if (response.headers['x-content-length']) {
                            app._status.len = +response.headers['x-content-length'];
                        } else {
                            app._status.len = -1;
                        }
                        app._status.loaded = 0;
                        app._status.t = Date.now();
                        gzip.on('data', function(chunk) {
                            size += chunk.length;
                            app._status.loaded = size;
                            data.push(chunk);
                        });
                        gzip.on('end', function(){
                            app._status.loaded = size;
                            resolve(Buffer.concat(data, size));
                        });
                    } else {
                        if (response.headers['content-length']) {
                            app._status.len = +response.headers['content-length'];
                        } else {
                            app._status.len = -1;
                        }
                        app._status.loaded = 0;
                        app._status.t = Date.now();
                        response.on('data', function(chunk) {
                            size += chunk.length;
                            app._status.loaded += chunk.length;
                            data.push(chunk);
                        });
                        response.on('end', function(){
                            app._status.loaded = size;
                            resolve(Buffer.concat(data, size));
                        });
                    }
                } else {
                    reject(response.statusCode);
                }
            }).on('error', reject);
        });
    },
    getStreamFromNet: function(url) {
        return new Promise((resolve, reject)=>{
            app._status.t_ping = Date.now();
            http.get(url, function(response) {
                app._status.t_pong = Date.now();
                if (response.statusCode === 200) {
                    if (response.headers['x-content-length'] || response.headers['content-length']) {
                        app._status.len = response.headers['x-content-length'] || response.headers['content-length'];
                        if (app._status.len === undefined) {
                            app._status.len = -1;
                        }
                    } else {
                        app._status.len = -1;
                    }
                    if (response.headers['content-encoding'] === 'gzip') {
                        const gzip = zlib.createGunzip();
                        stream.pipeline(response, gzip, (e)=>{e && reject(e)});
                        resolve(gzip);
                    } else {
                        resolve(response);
                    }
                } else {
                    reject(response.statusCode);
                }
            }).on('error', reject);
        });
    },
    loadIndexFromServer: async function() {
        return (await app.getFromNet('http://' + app.server + '/latest.index')).toString().split('\n').map(_=>{
            _=_.split(' '); 
            if (_[1] !== undefined) {
                return [
                    _[0], 
                    Buffer.from(_[0], 'base64').toString(), 
                    parseInt(_[1] || 0, 36), 
                    _[2]
                ]
            }
        });
    },
    toSeconds: function(hrtime, precision=3) {
        return (hrtime[0] + hrtime[1] / 1e9).toFixed(precision) + 's';
    },
    toTime: function(hrtime, precision=0) {
        if (typeof hrtime === 'number') {
            if (hrtime < 0 && -hrtime > 0) hrtime = -hrtime;
            hrtime = [hrtime / 1e3 | 0, 1e6 * (hrtime % 1e3)];
        } else if (hrtime instanceof Date) {
            hrtime = [hrtime.getTime() / 1e3 | 0, hrtime.getTime() % 1e3];
        }
        return (precision <= 6 && hrtime[0] > 86400 ? (hrtime[0] / 86400 % 7 | 0) + 'w ' : '') +
        (precision <= 5 && hrtime[0] > 3600 ? (hrtime[0] / 3600 % 24 | 0) + 'h ' : '') +
        (precision <= 4 && hrtime[0] > 60 ? (hrtime[0] / 60 % 60 | 0) + 'm ' : '') +
        (precision <= 3 && hrtime[0] >= 0 ? (hrtime[0] % 60 | 0) + 's ' : 
            (precision === 4 && hrtime[0] < 60 ? '<1m' : '')
        ) + 
        (precision <= 2 && hrtime[1] > 1e6 ? (hrtime[1] / 1e6 | 0) + 'ms ' : '') + 
        (precision <= 1 && hrtime[1] > 1e3 ? (hrtime[1] / 1e3 % 1e3 | 0) + 'μs ' : '') + 
        (precision <= 0 && hrtime[1] > 0 ? (hrtime % 1e3 | 0) + 'ns' : '');
    },
    // implemented in app.check
    //fullCheck: async function(event, message) {
    //    // mock
    //    event.sender.send('NodeMessage', {type: 'error', error: 'Method "fullCheck" is not implemented yet'});
    //},
    startXPlane: async function(event, message) {
        event.sender.send('NodeMessage', {type: 'error', error: 'Method "startXPlane" is not implemented yet'});
    },
    ready: function() {
        // remove defaul menu
        deskgap.app.setMenu(null);
        // create window and await for it's "ready" event
        const window = new deskgap.BrowserWindow({
            show: false,
            resizable: false,
            maximizable: false,
            frame: true,
            width: 800,
            height: 600,
            icon: 'resources/app/icon.ico'
        }).once('ready-to-show', ()=>{
            window.show();
            //fs.writeFile('/test.json', path.resolve('.'), function(e) {});
            // %TEMP%/...
        });
        if (process.platform !== 'win32') {
            window.webView.setDevToolsEnabled(true);
        }
        window.loadFile('index.html');
        window.on('closed', ()=>{
            window = null;
        });
    },
    getAllFilesList: async function(dir) {
        const dirents = await fs.promises.readdir(dir, { withFileTypes: true });
        const files = await Promise.all(dirents.map((dirent) => {
            const res = path.resolve(dir, dirent.name);
            return dirent.isDirectory() ? app.getAllFilesList(res) : res;
        }));
        return Array.prototype.concat(...files);
    },
    getSize: async function(filePath) {
        const stat = await fs.promises.stat(filePath);
        // stat.size
        return stat.size;
    },
    getRealHash: function(filePath, algorithm='sha1') {
        return new Promise((resolve, reject)=>{
            const hash = crypto.createHash(algorithm);
            const stream = fs.ReadStream(filePath);
            app._status.readed = 0;
            stream.on('data', (data)=>{
                app._status.readed += data.length;
                app._status.totalComplete += data.length;
                hash.update(data);
            });
            stream.on('end', ()=>{
                resolve(hash.digest('hex'));
            });
        });
    },
    consoleInit: function(args) {
        const xPath = path.resolve('.');
        const opts = {fast: false, full: true};
        if (args.indexOf('--fast') > -1) {
            opts.fast = true;
            opts.full = false;
        }
        // mocks
        console.log('Start ' + (opts.full?'full ':opts.fast?'fast ':'') + 'check');
        app.check({sender: {send: app.consoleMode.log}}, {path: xPath, checkType: (opts.fast ? 'fast' : opts.full ? 'full' : '')});
    },
    consoleMode: {
        log: function(eventType, msg) {
            if (['log', 'info', 'error', 'warn'].indexOf(msg.type) > -1) {
                process.stdout.write('\r\n');
                console[msg.type](msg[msg.type]);
                return;
            } else if (msg.type === 'status') {
                const rightPart = 
                    (typeof msg.status.speed === 'number'? msg.status.speed.toFixed(1) : msg.status.speed) + ' ' + msg.status.speedUnit +
                    ' ETE: ' + msg.status.ETA;
                process.stdout.write(
                    '\r' + utils.rightpad(utils.restrictRight(
                        msg.status.progress.all.value.toFixed(1) + '% ' + 
                        msg.status.progress.all.info + ' | ' + 
                        (msg.status.progress.current.value > -1 ? msg.status.progress.current.value.toFixed(1) + '% ': '') + 
                        msg.status.progress.current.info
                    , process.stdout.columns - rightPart.length - 1), process.stdout.columns - rightPart.length - 1, ' ') + rightPart
                );
            }
        }
    }
};

const fsCache = {
    cachePath: path.join(process.env.APPDATA, 'X11Updater'),
    read: async function(filePath) {
        return await fs.promises.readFile(path.join(fsCache.cachePath, filePath)).catch((e)=>{});
    },
    write: async function(filePath, data) {
        const fullPath = path.join(fsCache.cachePath, filePath);
        await fs.promises.mkdir(path.parse(fullPath).dir, {recursive: true});
        await fs.promises.writeFile(fullPath, data).catch((e)=>{});
        return true;
    }
};
const localStorage = {
    fileSystemPath: 'localStorage.json',
    get: async function(event, data) {
        const message = readJSONSafe(data);
        const rid = message.rid;
        localStorage._storage = readJSONSafe(await localStorage._storage_read()) || {};
        event.sender.send('LSP_storage_get', JSON.stringify({rid: rid, data: localStorage._storage}));
    },
    set: async function(event, data) {
        const message = readJSONSafe(data);
        const rid = message.rid;
        // rid = message.rid, storage = message.data
        localStorage._storage = message.data;
        await localStorage._storage_write();
        event.sender.send('LSP_storage_set', JSON.stringify({rid: rid, data: localStorage._storage}));
    },
    _storage: {},
    _enabled: false,
    _storage_read: async function() {
        return await fsCache.read(localStorage.fileSystemPath);
    },
    _storage_write: async function() {
        return await fsCache.write(localStorage.fileSystemPath, JSON.stringify(localStorage._storage));
    }
};

if (deskgap) {
    // subscribe to WebView window events
    deskgap.ipcMain.on('UIMessage', app.IPCResolver);
    deskgap.ipcMain.on('UIMessage_check', app.check);
    deskgap.ipcMain.on('UIMessage_startXPlane', app.startXPlane);
    deskgap.ipcMain.on('LSP_storage_get', localStorage.get);
    deskgap.ipcMain.on('LSP_storage_set', localStorage.set);
    deskgap.app.once('ready', app.ready);
} else {
    // or init console view
    app.consoleInit(process.argv.slice(2));
}