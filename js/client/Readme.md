# Remote-sync client

Yet-another synchronizer for your files with remote side

Originally written by [Irelynx](https://github.com/Irelynx) in November 2018 for synchronization of XPlane flight simulator mods folders

## Table of contents <a name="toc"></a>

0. [Table of contents](#toc)
1. [Build and run (simple ways)](#build_and_run_simple)
    1. [GUI](#build_and_run_simple.GUI)
    2. [Console](#build_and_run_simple.console)
        * [Console command line arguments](#build_and_run_simple.console.arguments)
2. [Client workflow (Or "How it works?")](#client_workflow)
    1. [GUI](#client_workflow.GUI)
    1. [Console](#client_workflow.console)
    1. [Synchronization modes (short explanation)](#client_workflow.synchronization_modes_short)
    1. [Full explanation of synchronization process](#client_workflow.full_sync_explanation)
        1. [`./latest.index` example](#client_workflow.latest_index_example)
        2. [Parsing of `./latest.index` file](#client_workflow.parsing_latest_index)
        3. [Rules of populating the `download list`](#client_workflow.populating_download_list)
        4. [Downloading data according to `download list`](#client_workflow.downloading_process)
    1. [Schematic view of synchronization process](#client_workflow.schematic_process_workflow)
3. [Used software](#used_software)
    

## Build and run (simple ways) <a name="build_and_run_simple"></a>

### GUI <a name="build_and_run_simple.GUI"></a>

1. "Build" with `build.bat`
2. Copy `updater.exe` to target machine and run it

### Console <a name="build_and_run_simple.console"></a>

0. Install Node.js (or Copy `node.exe` to target location. Tested on Node.js v12.14)
1. Copy `main.js` and `utils.js` from `src/resources/app/` to target location
2. Run `node ./main.js` (after running `main.js`, files will be downloaded to )

#### Command line arguments <a name="build_and_run_simple.console.arguments"></a>

| Argument | Description |
|-|-|
| `--fast` | Toggle `fast` mode (by default, in console variant, `full` mode will be used) |

## Client workflow (Or "How it works?") <a name="client_workflow"></a>

GUI and Console ways of usage use same `main.js` file..

### GUI <a name="client_workflow.GUI"></a>

App uses [DeskGap (npm)](https://www.npmjs.com/package/deskgap) to provide sufficient graphical user interface with **smallest (6 MB zipped, 22 MB unzipped) distribution size than Electron or NW.JS (70+ MB zipped)**.

DeskGap uses something like WebView in Android for rendering HTML, CSS, and browser-side JavaScript. Also, DeskGap provides a "bridge" to Node.js instance, which is more than enough to create apps (but with some restrictions and bugs.. f.e., you can't use a debugger by default)

App, on start, asks user for local directory, which needs to be synchronized with remote server specified in source code (see `app.server` field in `main.js`)

Then, user can choose one of two synchronization modes: Fast or Full

[See more about synchronization modes here](#client_workflow.synchronization_modes_short)


### Console <a name="client_workflow.console"></a>

For console way of usage, you can just run `main.js` via Node.js and it will start the synchronization process in current working directory with remote server specified in source code (see `app.server` field in `main.js`)

User can choose one of two synchronization modes: Fast or Full, but by default, full mode will be used (can be overridden using [command line arguments](#build_and_run_simple.console.arguments))

[See more about synchronization modes here](#client_workflow.synchronization_modes_short)

### Synchronization modes (short explanation) <a name="client_workflow.synchronization_modes_short"></a>

| Mode | Description |
|-|-|
| **Fast** | <li> Fetch file list from server, <li> then find each file in chosen directory. <li> To ensure the integrity of each file, in this mode, will be used **size comparing**. <li> If target size is not equal to size received from server, <li> OR if target is not exists <li> then app will create new file/folders, and download (overwrite if exists) the target file. |
| **Full** | <li> Fetch file list from server, <li> then find each file in chosen directory. <li> To ensure the integrity of each file, in this mode, will be used **size comparing** AND **hash sum calculation**. <li> If target size is not equal to size received from server, <li> OR if hash sum is not equal to hash sum receiver from server, <li> OR if target is not exists, <li> then app will create new file/folders and download (overwrite if exists) the target file. |

### Full explanation of synchronization process <a name="client_workflow.full_sync_explanation"></a>

1. First of all, client recursively get files `relative paths` (to chosen directory) and `sizes` from filesystem to populate `local files index`
2. Using `HTTP` protocol, `client` downloads `./latest.index` file from `remote server`.
    
    Example of `latest.index` file: <a name="client_workflow.latest_index_example"></a>
    ```
    QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBLmFjZg== 1jcfb 0a15df640e2553255f0ebaaacc8bbe4572f66612
    QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBX2ljb24ucG5n 1m27 e9259948055c96580acc58daf238520c7c0434a9
    QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBX2ljb24xMS5wbmc= 3j3p 6c7cb625bd1523aa3630eb8455eb100fc100064f
    QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBX2ljb24xMV90aHVtYi5wbmc= 8tj 301b01ee07f9a9b4eadcf54445f033f3e72ef250
    ```
    
    Parsing of `./latest.index` file is simple enough: <a name="client_workflow.parsing_latest_index"></a>
    * Each row (delimiter - `\n` (line feed)) contains 3 fields: `filePath`, `size` and `hash`, delimited by one space (` `)
    * `filePath` appears first, and it is Base64 encoded. 
        First row from example can be decoded like this: 
        ```javascript
        Buffer.from('QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBLmFjZg==', 'base64').toString()
        // -> 'Aircraft/Extra Aircraft/B-52G NASA/B-52G NASA.acf'
        Buffer.from('Aircraft/Extra Aircraft/B-52G NASA/B-52G NASA.acf').toString('base64')
        // -> 'QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBLmFjZg=='
        ```
        **NOTE**: `filePath` is always use UNIX-style directory delimiters (forward slash - `/`)

    * `size` in `latest.index` is an Base36 encoded number of Bytes (file size).
        First row from example can be decoded like this:
        ```javascript
        parseInt('1jcfb', 36);
        // -> 2582183 (so, file size is approximately 2.46 MB)
        2582183.toString(36);
        // -> '1jcfb'
        ```
        
        **NOTE**: If `size` is empty, then it must be interpreted as `0` or `Infinity` (`size` can't be determined, or client must re-download this file each time)

    * `hash` is just hash in hexadecimal format. In example (`0a15df640e2553255f0ebaaacc8bbe4572f66612`), **sha1** is used (algorithm can be changed in source code).
        
        **NOTE**: If `hash` is empty, then it must be interpreted as empty string (`hash` can't be determined, or client must re-download this file each time)

3. After fetching and parsing of `./latest.index` file, <a name="client_workflow.populating_download_list"></a> client ensure integrity of all files, that contains in `./latest.index` and also in `local files index` with same relative path names, and based on result of integrity checks populates the `download list`:
    * If file is NOT in `local files index` - it must be appended to `download list`
    * If mode is `Fast` or `Full`, and `size` in `./latest.index` does NOT equals to `size` in `local files index` - it must be appended to `download list`
    * If mode is `Full`, and `size` in `./latest.index` equals to `size` in `local files index`, and `hash` in `./latest.index` does NOT equals to calculated in-place hash of file from `local files index` - it must be appended to `download list`
4. Downloading data according to `download list`: <a name="client_workflow.downloading_process"></a>
    1. for each file in `download list`, `Client` do `HTTP GET` request to `remote server`.
        F.e.: `client` want to get contents of `Aircraft/Extra Aircraft/B-52G NASA/B-52G NASA.acf`, so `client` must do following `HTTP GET` request: `/QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBLmFjZg==`
        
        **NOTE**: request also can include parent directory (for cases where the `remote server` located behind reverse proxy like Nginx): `/syncRemoteServerPath/QWlyY3JhZnQvRXh0cmEgQWlyY3JhZnQvQi01MkcgTkFTQS9CLTUyRyBOQVNBLmFjZg==`
    2. If in `HTTP` headers appears `content-encoding` header with value equals to `gzip` - `client` must decompress `response stream` using `ZLib gunzip`
    3. Each decompressed (or raw) chunk from `response stream` can be written to target `filePath` after decompression (or immediately, if raw)

### Schematic view of synchronization process <a name="client_workflow.schematic_process_workflow"></a>

```mermaid

sequenceDiagram
    participant FS as File System
    participant C as Client
    participant S as Server
    Note over FS,C: File system interaction
    Note over C,S: Network interaction (via HTTP)
    rect rgba(0, 0, 255, 0.1)
        loop Recursively get file paths and size
            C->>+FS: Get directory entries
            FS->>-C: Directory entries
            loop Get files size
                C->>+FS: 
                FS->>-C: File size
            end
            Note right of FS: Repeat for each directory entry recursively
        end
    end
    rect rgba(255, 127, 0, 0.1)
        Note right of C: Loading and parsing ./latest.index
        C->>+S: HTTP GET /latest.index
        S->>-C: /latest.index contents
        C-->>C: latest.index parsing
    end
    rect rgba(0, 0, 0, 0.1)
        Note left of C: Integrity checks, population of "download list"
        C-->>C: Size compare
        alt only in Full check
            C-->>+FS: hash-sum calculation
            FS-->>-C: 
        end
    end
    rect rgba(0, 255, 0, 0.1)
        Note over FS,S: Data transfer
        loop for each file in download list
            C->>+S: HTTP GET /QWlyY3JhZnQvR...
            S-->>C: HTTP headers
            C-->>FS: Create write stream (open file to write)
            FS-->>C: 
            loop
                S->>C: Chunk
                alt content-encoding == gzip
                    C-->>C: Stream decompressing
                    C-->>FS: Write (can be delayed by decompressor)
                else else
                    C->>FS: Write
                end
            end
            S->>-C: Connection close (Done)
            C-->>FS: Close write stream (close file descriptor)
            FS-->>C: 
        end
    end
```

## Used Software <a name="used_software"></a>

* [Node.js](https://nodejs.org/) - scripting "Core"
* [DeskGap](https://www.npmjs.com/package/deskgap) - "WebView" + Node.js
* [7z GUI](https://www.7-zip.org/download.html) (`7zG.exe`) and 7z SFX (from 7z extras) - to pack into single executable, self-extracted, file
* [rcedit](https://github.com/electron/rcedit) ([alt link](https://github.com/electron/node-rcedit)) - to change icon and some other meta-data of 7z SFX extractor and DeskGap.exe (pretty name, description, icon, version and other info in windows explorer and task-manager)
* [Manifest Tool](https://docs.microsoft.com/en-us/cpp/build/how-to-embed-a-manifest-inside-a-c-cpp-application?view=msvc-160) (from Microsoft Visual Studio) - to change manifest of executable files to prevent starting in too old systems (Windows XP)
* [Bluebird](https://www.npmjs.com/package/bluebird) - promises polyfill for Internet Explorer.....
* [Materialize CSS](http://materializecss.com/) - aka Bootstrap, but with Material Design concepts
    * [Material Icons](https://material.io/resources/icons/?style=baseline)